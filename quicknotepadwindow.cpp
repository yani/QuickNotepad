/*--------------------------------------------------------------------
 * Copyright (C) 2018, Nexfloat
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 * April 2018, Mexico City, MEX
 *--------------------------------------------------------------------
*/

/* @file quicknotepadwindow.cpp
 * @brief Contains the method and variables definition of the Quick Notepad application.
 *
 * This is a Quick Notepad application developed in Qt intended for any use. This will be
 * improved and released gradually.
 *
 * @author YAN
*/
#include "quicknotepadwindow.h"
#include "ui_quicknotepadwindow.h"

QuicknotepadWindow::QuicknotepadWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::QuicknotepadWindow)
{
    ui->setupUi(this);
    //this->setCentralWidget(ui->textEdit);
    readSettings();


}

QuicknotepadWindow::~QuicknotepadWindow()
{
    delete ui;
}

void QuicknotepadWindow::on_actionNew_triggered()
{
    qsFileName = "";
    ui->textEdit->setPlainText("");
}

void QuicknotepadWindow::on_actionCopy_triggered()
{
    ui->textEdit->copy();
}

void QuicknotepadWindow::on_actionPaste_triggered()
{
    ui->textEdit->paste();
}

void QuicknotepadWindow::on_actionCut_triggered()
{
    ui->textEdit->cut();
}

void QuicknotepadWindow::on_actionUndo_triggered()
{
    ui->textEdit->undo();
}

void QuicknotepadWindow::on_actionRedo_triggered()
{
    ui->textEdit->redo();
}

void QuicknotepadWindow::on_actionOpen_triggered()
{
    QString qsFile = QFileDialog::getOpenFileName(this, "Open a file");
    if(!qsFile.isEmpty()){
        QFile fFile(qsFile);
        if(fFile.open( QFile::ReadOnly | QFile::Text )){
            qsFileName = qsFile;    ///< set the class' variable to the opened path
            QTextStream qtsIn(&fFile);
            QString qsText = qtsIn.readAll();
            fFile.close();
            ui->textEdit->setPlainText(qsText);
            this->setWindowTitle("Quick Notepad - "+qsFileName);
        }
    }

}

void QuicknotepadWindow::on_actionSave_triggered()
{
    if(qsFileName.isEmpty()){
        on_actionSave_as_triggered();
    }else{
        QFile qfFile(qsFileName);
        if(qfFile.open(QFile::WriteOnly | QFile::Text)){
            QTextStream qtsOut(&qfFile);

            qtsOut<<ui->textEdit->toPlainText();

            qfFile.flush();
            qfFile.close();
        }
    }
    this->setWindowTitle("Quick Notepad - "+qsFileName);
}

void QuicknotepadWindow::on_actionSave_as_triggered()
{
    /// it is similar to open action, however we change the getOpenFileName by getSaveFileName
    QString qsFile = QFileDialog::getSaveFileName(this, "Save file");
    if(!qsFile.isEmpty()){
        /// we call to on_actionSave_triggered
        qsFileName = qsFile;
        on_actionSave_triggered();
        this->setWindowTitle("Quick Notepad - "+qsFileName);
    }
}

void QuicknotepadWindow::readSettings()
{
    QSettings settings("Quick Notepad", "Quick Notepad");
    QPoint pos = settings.value("pos", QPoint(100,100)).toPoint();
    QSize size = settings.value("size", QPoint(3000,3000)).toSize();
    resize(size);
    move(pos);
    this->setWindowTitle("Quick Notepad");
}

void QuicknotepadWindow::writeSettings()
{

    QSettings settings("Quick Notepad", "Quick Notepad");
    settings.setValue("pos", pos());
    settings.setValue("size", size());
}

void QuicknotepadWindow::closeEvent(QCloseEvent *event)
{
    if (maybeSave()) {
        writeSettings();
        event->accept();
    } else {
        event->ignore();
    }
}

bool QuicknotepadWindow::maybeSave()
{
    if (!ui->textEdit->document()->isModified())
        return true;
    const QMessageBox::StandardButton ret
        = QMessageBox::warning(this, tr("Quick Notepad - Warning"),
                               tr("The document has been modified.\n"
                                  "Do you want to save your changes?"),
                               QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
    switch (ret) {
        case QMessageBox::Save:
            save();
            if(qsFileName.isEmpty()){
                return false;
            }else{
                return true;
            }
        case QMessageBox::Cancel:
            return false;
        default:
            break;
    }
    return true;
}

void QuicknotepadWindow::save()
{
    if (qsFileName.isEmpty()) {
        on_actionSave_as_triggered();
    } else {
        on_actionSave_triggered();
    }
}

void QuicknotepadWindow::on_btnFindPrevious_clicked()
{
    ui->textEdit->find(ui->lineEditSearch->text());
}

void QuicknotepadWindow::on_btnFindForward_clicked()
{
    ui->textEdit->find(ui->lineEditSearch->text(), QTextDocument::FindBackward);
}

void QuicknotepadWindow::on_actionAbout_triggered()
{
    QMessageBox::information(this,"About Quick Notepad","2018. Nexfloat\n"
                                            "Quick Notepad is free software under the terms of the "
                                            "GNU General Public License version 3.\n\n"
                                            "Credits:\n\nProgramming\n----------------\n"
                                            "Yani Miguel");
}

void QuicknotepadWindow::on_actionAbout_QT_triggered()
{
    QMessageBox::information(this,"About Qt","Powered by Qt technology and C++.\n\n"
                                             "Recognition to:\n"
                                             "- The Qt Company\n"
                                             "- Bjarne Stroustrup by his contribution on creating the C++ programming language");
}
