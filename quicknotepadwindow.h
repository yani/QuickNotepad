/*--------------------------------------------------------------------
 * Copyright (C) 2018, Nexfloat
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 * April 2018, Mexico City, MEX
 *--------------------------------------------------------------------
*/

/* @file quicknotepadwindow.cpp
 * @brief Contains the method and variables definition of the Quick Notepad application.
 *
 * This is a Quick Notepad application developed in Qt intended for any use. This will be
 * improved and released gradually.
 *
 * @author YAN
*/

#ifndef QUICKNOTEPADWINDOW_H
#define QUICKNOTEPADWINDOW_H

#include <QMainWindow>
#include <QtCore>
#include <QtGui>
#include <QFileDialog>
#include <QFile>
#include <QSettings>
#include <QMessageBox>

namespace Ui {
class QuicknotepadWindow;
}

class QuicknotepadWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit QuicknotepadWindow(QWidget *parent = 0);
    ~QuicknotepadWindow();

protected:
    void closeEvent(QCloseEvent *event) override;

private slots:
    void on_actionOpen_triggered();

    void on_actionCopy_triggered();

    void on_actionNew_triggered();

    void on_actionPaste_triggered();

    void on_actionCut_triggered();

    void on_actionUndo_triggered();

    void on_actionRedo_triggered();

    void on_actionSave_triggered();

    void on_actionSave_as_triggered();

    bool maybeSave();

    void save();

    void on_btnFindPrevious_clicked();

    void on_btnFindForward_clicked();

    void on_actionAbout_triggered();

    void on_actionAbout_QT_triggered();

private:
    Ui::QuicknotepadWindow *ui;
    QString qsFileName;     ///< used to store the file name

    /// functions to read settings
    void readSettings();
    void writeSettings();

};

#endif // QUICKNOTEPADWINDOW_H
