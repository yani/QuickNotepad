/*--------------------------------------------------------------------
 * Copyright (C) 2018, Nexfloat
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 * April 2018, Mexico City, MEX
 *--------------------------------------------------------------------
*/

/* @file quicknotepadwindow.cpp
 * @brief Contains the method and variables definition of the Quick Notepad application.
 *
 * This is a Quick Notepad application developed in Qt intended for any use. This will be
 * improved and released gradually.
 *
 * @author YAN
*/
#include "quicknotepadwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QuicknotepadWindow w;
    w.show();

    return a.exec();
}
